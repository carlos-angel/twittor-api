import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigType } from '@nestjs/config';

import { AuthController } from '@authModule/auth.controller';
import { AuthService } from '@authModule/services/auth.service';
import { UserService } from '@authModule/services/user.service';
import { User, UserSchema } from '@authModule/entities/user.entity';
import { LocalStrategy } from '@authModule/strategies/local.strategy';
import { JwtStrategy } from '@authModule/strategies/jwt.strategy';
import { EncoderService } from '@authModule/services/encoder.service';
import config from '@src/config';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      inject: [config.KEY],
      useFactory: (configService: ConfigType<typeof config>) => ({
        secret: configService.jwtSecret,
        signOptions: {
          expiresIn: configService.jwtExpiresIn,
        },
      }),
    }),
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
    ]),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    UserService,
    LocalStrategy,
    JwtStrategy,
    EncoderService,
  ],
  exports: [UserService, EncoderService],
})
export class AuthModule {}
