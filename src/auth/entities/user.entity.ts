import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ExcludeProperty } from 'nestjs-mongoose-exclude';

@Schema()
export class User extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true, unique: true })
  email: string;

  @Prop({ required: true })
  @ExcludeProperty()
  password: string;

  @Prop({ required: false })
  birthday: Date;

  @Prop({ required: false })
  biography: string;

  @Prop({ required: false })
  avatar: string;

  @Prop({ required: false })
  banner: string;

  @Prop({ required: false })
  website: string;

  @Prop({ required: false })
  location: string;

  @Prop({ required: false })
  resetPasswordToken: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
