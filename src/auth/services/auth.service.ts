import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '@authModule/services/user.service';
import { EncoderService } from '@authModule/services/encoder.service';
import { User } from '@authModule/entities/user.entity';
import { PayloadToken } from '@authModule/models/token.model';
import { RequestResetPasswordDto } from '@authModule/dtos/request-reset-password.dto';
import { ResetPasswordDto } from '@authModule/dtos/reset-password.dto';
import { ChangePasswordDto } from '@authModule/dtos/change-password.dto';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private encoderService: EncoderService,
  ) {}

  async validateUser(email: string, password: string): Promise<User> {
    const user = await this.userService.findByEmailOrFail(email);
    const isMatch = await this.encoderService.isMatchPasswords(
      password,
      user.password,
    );
    if (!isMatch) {
      throw new UnauthorizedException('email or password incorrect');
    }
    return user;
  }

  generateJWT(user: User) {
    const payload: PayloadToken = {
      sub: user.id,
      name: user.name,
      email: user.email,
    };
    return {
      token: this.jwtService.sign(payload),
    };
  }

  async requestResetPassword(
    requestResetPasswordDto: RequestResetPasswordDto,
  ): Promise<void> {
    const { email } = requestResetPasswordDto;
    await this.userService.requestResetPasswordToken(email);
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto): Promise<void> {
    const { resetPasswordToken, password } = resetPasswordDto;
    await this.userService.updatePasswordByResetPasswordToken(
      resetPasswordToken,
      password,
    );
  }

  async changePassword(
    id: string,
    changePasswordDto: ChangePasswordDto,
  ): Promise<void> {
    const { oldPassword, newPassword } = changePasswordDto;

    const user = await this.userService.findByIdOrFail(id);

    if (
      await this.encoderService.isMatchPasswords(oldPassword, user.password)
    ) {
      await this.userService.updatePasswordById(id, newPassword);
    } else {
      throw new BadRequestException('Old password does not match');
    }
  }
}
