import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { v4 } from 'uuid';
import { User } from '@authModule/entities/user.entity';
import { CreateUserDto } from '@authModule/dtos/user.dto';
import { EncoderService } from '@authModule/services/encoder.service';
import { UpdateProfileDto } from '@profileModule/dtos/update-profile.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    private encodeService: EncoderService,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<void> {
    const user = new this.userModel(createUserDto);
    user.password = await this.encodeService.encoderPassword(
      createUserDto.password,
    );
    try {
      await user.save();
    } catch (e) {
      if (e.code === 11000) {
        throw new ConflictException('this email is already registered');
      }

      throw new InternalServerErrorException();
    }
  }

  async findByNameQuery(query: string): Promise<User[]> {
    const users = await this.userModel
      .find({ name: { $regex: '.*' + query + '.*' } })
      .limit(10)
      .exec();

    return users;
  }

  async findByEmailOrFail(email: string): Promise<User> {
    const user = await this.userModel.findOne({ email });
    if (!user) {
      throw new UnauthorizedException('email or password incorrect');
    }

    return user;
  }

  async findByIdOrFail(id: string): Promise<User> {
    const user = await this.userModel.findById(id);
    if (!user) {
      throw new NotFoundException('profile not found');
    }

    return user;
  }

  async update(id: string, changes: UpdateProfileDto): Promise<void> {
    await this.userModel
      .findByIdAndUpdate(id, { $set: changes }, { new: false })
      .exec();
  }

  async requestResetPasswordToken(email: string): Promise<void> {
    await this.userModel
      .findOneAndUpdate(
        { email },
        { $set: { resetPasswordToken: v4() } },
        { new: false },
      )
      .exec();
    //TODO: Send email (e.g. Dispatch an event so MailerModule can send the email)
  }

  async updatePasswordByResetPasswordToken(
    resetPasswordToken: string,
    newPassword: string,
  ): Promise<void> {
    const password = await this.encodeService.encoderPassword(newPassword);

    await this.userModel
      .findOneAndUpdate(
        { resetPasswordToken },
        { $set: { password, resetPasswordToken: null } },
        { new: false },
      )
      .exec();
  }

  async updatePasswordById(id: string, newPassword: string): Promise<void> {
    const password = await this.encodeService.encoderPassword(newPassword);

    await this.userModel
      .findByIdAndUpdate(id, { $set: { password } }, { new: false })
      .exec();
  }

  async updateAvatar(id: string, avatar: string): Promise<void> {
    await this.userModel
      .findByIdAndUpdate(id, { $set: { avatar } }, { new: false })
      .exec();
  }

  async updateBanner(id: string, banner: string): Promise<void> {
    await this.userModel
      .findByIdAndUpdate(id, { $set: { banner } }, { new: false })
      .exec();
  }
}
