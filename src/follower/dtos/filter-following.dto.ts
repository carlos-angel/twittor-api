import { IsNumber, Min } from 'class-validator';

export class FilterFollowingDto {
  @IsNumber()
  @Min(1)
  page: number;
}
