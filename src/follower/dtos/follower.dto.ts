import { IsMongoId, IsString, IsUrl } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FollowerDto {
  @IsMongoId()
  @ApiProperty()
  readonly id: string;

  @IsString()
  @ApiProperty()
  readonly name: string;

  @IsUrl()
  @ApiProperty()
  readonly avatar: string;

  @IsString()
  @ApiProperty()
  readonly biography: string;
}
