import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from '@authModule/entities/user.entity';

@Schema()
export class Follower extends Document {
  @Prop({ type: Types.ObjectId, ref: User.name })
  follower: Types.ObjectId | User;

  @Prop({ type: Types.ObjectId, ref: User.name })
  following: Types.ObjectId | User;
}

export const FollowerSchema = SchemaFactory.createForClass(Follower);
