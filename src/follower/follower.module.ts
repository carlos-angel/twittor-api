import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FollowerService } from '@followerModule/services/follower.service';
import { FollowerController } from '@followerModule/follower.controller';
import { AuthModule } from '@authModule/auth.module';
import {
  Follower,
  FollowerSchema,
} from '@followerModule/entities/follower.entity';

@Module({
  imports: [
    AuthModule,
    MongooseModule.forFeature([
      {
        name: Follower.name,
        schema: FollowerSchema,
      },
    ]),
  ],
  providers: [FollowerService],
  controllers: [FollowerController],
  exports: [FollowerService],
})
export class FollowerModule {}
