import { BadRequestException } from '@nestjs/common';
import { Request } from 'express';
import { v4 } from 'uuid';

export const renameImage = (
  req: Request,
  file: Express.Multer.File,
  callback,
) => {
  const typeFile = file.originalname.split('.')[1];
  const name = v4();

  callback(null, `${name}.${typeFile}`);
};

export const fileFilterImage = (
  req: Request,
  file: Express.Multer.File,
  callback,
) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(new BadRequestException('Invalid format type'), false);
  }
  callback(false, true);
};
