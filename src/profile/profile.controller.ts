import {
  Controller,
  Get,
  Put,
  Body,
  Param,
  UseInterceptors,
  UseGuards,
  Post,
  UploadedFile,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { SanitizeMongooseModelInterceptor } from 'nestjs-mongoose-exclude';
import { ProfileService } from '@profileModule/services/profile.service';
import { MongoIdPipe } from '@common/mongo-id.pipe';
import { JwtAuthGuard } from '@authModule/guards/jwt-auth.guard';
import { UpdateProfileDto } from '@profileModule/dtos/update-profile.dto';
import { PayloadToken } from '@authModule/models/token.model';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { renameImage, fileFilterImage } from '../helper/imageHelper';
import { AvatarUploadDto } from '@profileModule/dtos/avatar-upload.dto';
import { BannerUploadDto } from '@profileModule/dtos/banner-upload.dto';
import { TweetService } from '@tweetModule/services/tweet.service';
import { FilterTweetsDto } from '@tweetModule/dtos/filter-tweets.dto';
import { FilterFollowingDto } from '../follower/dtos/filter-following.dto';
import { FollowerService } from '../follower/services/follower.service';
import { GetPayloadToken } from '@authModule/decorators/get-payload-token.decorator';
import { FilterTweetsFromFollowingsDto } from '@tweetModule/dtos/filter-tweets-followings.dto';

@UseGuards(JwtAuthGuard)
@UseInterceptors(
  new SanitizeMongooseModelInterceptor({
    excludeMongooseId: false,
    excludeMongooseV: true,
  }),
)
@ApiTags('profile')
@Controller('profile')
export class ProfileController {
  constructor(
    private profileService: ProfileService,
    private tweetService: TweetService,
    private followerService: FollowerService,
  ) {}

  @Get('/tweets/followings')
  tweetsFromFollowings(
    @Query() filterTweetsFromFollowings: FilterTweetsFromFollowingsDto,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    return this.followerService.tweetsFromFollowings(
      payloadToken.sub,
      filterTweetsFromFollowings,
    );
  }

  @Get('/search')
  searchProfiles(@Query('query') query: string) {
    return this.profileService.searchProfiles(query);
  }

  @Get(':id')
  get(@Param('id', MongoIdPipe) id: string) {
    return this.profileService.getProfileById(id);
  }

  @Put()
  update(
    @Body() changes: UpdateProfileDto,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    this.profileService.updateProfile(payloadToken.sub, changes);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: AvatarUploadDto,
  })
  @Post('upload-avatar')
  @UseInterceptors(
    FileInterceptor('avatar', {
      storage: diskStorage({
        destination: './uploads/avatar',
        filename: renameImage,
      }),
      fileFilter: fileFilterImage,
    }),
  )
  uploadAvatar(
    @UploadedFile() file: Express.Multer.File,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    this.profileService.updateAvatar(payloadToken.sub, file);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: BannerUploadDto,
  })
  @Post('upload-banner')
  @UseInterceptors(
    FileInterceptor('banner', {
      storage: diskStorage({
        destination: './uploads/banner',
        filename: renameImage,
      }),
      fileFilter: fileFilterImage,
    }),
  )
  uploadBanner(
    @UploadedFile() file: Express.Multer.File,
    @GetPayloadToken() payloadToken: PayloadToken,
  ) {
    this.profileService.updateBanner(payloadToken.sub, file);
  }

  @Get(':id/tweets')
  tweets(
    @Param('id', MongoIdPipe) id: string,
    @Query() filterTweetsDto: FilterTweetsDto,
  ) {
    return this.tweetService.tweetsByUserId(id, filterTweetsDto);
  }

  @Get(':id/followings')
  followings(
    @Param('id', MongoIdPipe) id: string,
    @Query() filterFollowingDto: FilterFollowingDto,
  ) {
    return this.followerService.getFollowingsByUserId(id, filterFollowingDto);
  }

  @Get(':id/followers')
  followers(
    @Param('id', MongoIdPipe) id: string,
    @Query() filterFollowingDto: FilterFollowingDto,
  ) {
    return this.followerService.getFollowersByUserId(id, filterFollowingDto);
  }
}
