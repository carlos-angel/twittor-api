import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, IsNotEmpty } from 'class-validator';

export class CreateTweetDto {
  @IsNotEmpty()
  @MaxLength(280, { message: 'the tweet cannot be more than 280 characters ' })
  @ApiProperty()
  readonly message: string;
}
