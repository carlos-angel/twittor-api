import { IsNumber, Min, IsString, IsIn, IsOptional } from 'class-validator';

export class FilterTweetsFromFollowingsDto {
  @IsNumber()
  @Min(1)
  page: number;

  @IsOptional()
  @IsString()
  @IsIn(['asc', 'des'])
  order: string;
}
