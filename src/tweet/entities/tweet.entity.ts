import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from '@authModule/entities/user.entity';

@Schema()
export class Tweet extends Document {
  @Prop({ type: Types.ObjectId, ref: User.name })
  user: Types.ObjectId | User;

  @Prop({ required: true })
  message: string;

  @Prop({ required: true, default: new Date() })
  published: Date;

  @Prop({ required: true, default: true })
  active: boolean;
}

export const TweetSchema = SchemaFactory.createForClass(Tweet);
